import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {timer} from "rxjs";
import {Observable} from "rxjs/internal/Observable";

@Component({
    templateUrl: './aircondition.component.html'
})
export class AirconditionComponent {
    action: number;
    currentTemperature: number;
    desiredTemperature: number = 20;
    date: string;
    time: string;
    $source: Observable<number>;

    constructor() {
        this.currentTemperature = 20;
        this.desiredTemperature = 20;
        this.updateAction()

        this.$source = timer(0, 60000);
        this.$source.subscribe(() => {
            this.date = new Date().toLocaleDateString();
            this.time = new Date().toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit'
            });
        });
    }

    change(value: string) {
        let parseFloat = Number.parseFloat(value);
        if (!Number.isNaN(parseFloat)) {
            this.currentTemperature = parseFloat;
            this.updateAction();
        }
    }

    private updateAction() {
        if (this.currentTemperature <= this.desiredTemperature + 1 &&
            this.desiredTemperature - 1 <= this.currentTemperature) {
            this.action = 0;
        } else if (this.currentTemperature > this.desiredTemperature) {
            this.action = -1;
        } else if (this.currentTemperature < this.desiredTemperature) {
            this.action = 1;
        }
    }

    plus() {
        if (this.desiredTemperature < 30) {
            this.desiredTemperature++;
            this.updateAction();
        }
    }

    minus() {
        if (this.desiredTemperature > 15) {
            this.desiredTemperature--;
            this.updateAction();
        }
    }
}
